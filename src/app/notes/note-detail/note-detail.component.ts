import { Component, Input, OnInit } from '@angular/core';

import { NotesService } from '../notes.service';

@Component({
  selector: 'app-note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.scss'],
})
export class NoteDetailComponent implements OnInit {

  @Input() note: any;

  constructor(private notesService: NotesService) { }


  ngOnInit() {
  }

  addHeartToNote(val: number) {
    if (this.note.id) {
      this.notesService.updateNote(this.note.id, { hearts: val + 1 });
    } else {
      console.error('Note missing ID!');
    }
  }

  deleteNote(id: string) {
    this.notesService.deleteNote(id);
  }

}
